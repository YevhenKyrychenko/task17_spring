package com.kyrychenko.task17_spring.beans;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

@Getter
@Setter
public class BeanA implements BeanValidator, InitializingBean, DisposableBean {
    private String name;
    private int value;

    @Override
    public void validate(Object obj, Errors e) {
        ValidationUtils.rejectIfEmpty(e, "name", "name.empty");
        ValidationUtils.rejectIfEmpty(e, "value", "value.empty");
        BeanA bean = (BeanA) obj;
        if (bean.value < 0) {
            e.rejectValue("age", "negative value");
        } else if (bean.name == null) {
            e.rejectValue("name", "wrong name");
        }
    }

    @Override
    public String toString() {
        return "BeanA{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("inside DisposableBean.destroy()");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("inside InitializingBean.afterPropertiesSet()");
    }
}
