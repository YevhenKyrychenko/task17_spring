package com.kyrychenko.task17_spring.beans;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

@Getter
@Setter
public class BeanF implements BeanValidator {
    private String name;
    private int value;

    @Override
    public void validate(Object obj, Errors e) {
        ValidationUtils.rejectIfEmpty(e, "name", "name.empty");
        ValidationUtils.rejectIfEmpty(e, "value", "value.empty");
        BeanF bean = (BeanF) obj;
        if (bean.value < 0) {
            e.rejectValue("age", "negative value");
        } else if (bean.name == null) {
            e.rejectValue("name", "wrong name");
        }
    }

    @Override
    public String toString() {
        return "BeanF{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
