package com.kyrychenko.task17_spring.beans;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Getter
@Setter
public class BeanE implements BeanValidator {
    private String name;
    private int value;

    @Override
    public void validate(Object obj, Errors e) {
        ValidationUtils.rejectIfEmpty(e, "name", "name.empty");
        ValidationUtils.rejectIfEmpty(e, "value", "value.empty");
        BeanE bean = (BeanE) obj;
        if (bean.value < 0) {
            e.rejectValue("age", "negative value");
        } else if (bean.name == null) {
            e.rejectValue("name", "wrong name");
        }
    }

    @PostConstruct
    public void annoInitMethod() {
        System.out.println("inside @PostConstruct-method");
    }

    @PreDestroy
    public void annoDestroyMethod() {
        System.out.println("inside @PreDestroy-method");
    }


    @Override
    public String toString() {
        return "BeanE{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
