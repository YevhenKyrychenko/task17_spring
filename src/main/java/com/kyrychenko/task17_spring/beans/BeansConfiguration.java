package com.kyrychenko.task17_spring.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;

@Configuration
@Import(BeansSecondConfiguration.class)
public class BeansConfiguration {
    @Bean("beanA")
    public BeanA beanA() {
        return new BeanA();
    }

    @Bean("beanAFirst")
    public BeanA beanAFirst() {
        BeanA beanA = new BeanA();
        beanA.setName(beanB().getName());
        beanA.setValue(beanC().getValue());
        return beanA;
    }

    @Bean("beanASecond")
    public BeanA beanASecond() {
        BeanA beanA = new BeanA();
        beanA.setName(beanB().getName());
        beanA.setValue(beanD().getValue());
        return beanA;
    }

    @Bean("beanAThird")
    public BeanA beanAThird() {
        BeanA beanA = new BeanA();
        beanA.setName(beanD().getName());
        beanA.setValue(beanC().getValue());
        return beanA;
    }

    @Bean(name = "beanB", initMethod = "customInitMethod",
            destroyMethod = "customDestroyMethod")
    public BeanB beanB() {
        return new BeanB();
    }

    @Bean(name = "beanC",initMethod = "customInitMethod",
            destroyMethod = "customDestroyMethod")
    @DependsOn(value = "{beanB}")
    public BeanC beanC() {
        return new BeanC();
    }

    @Bean(name = "beanD",initMethod = "customInitMethod",
            destroyMethod = "customDestroyMethod")
    @DependsOn(value = "{beanB, beanC}")
    public BeanD beanD() {
        return new BeanD();
    }

    @Bean
    public BeanE beanE() {
        return new BeanE();
    }

    @Bean("beanEFirst")
    public BeanE beanEFirst() {
        BeanE beanE = new BeanE();
        beanE.setName(beanAFirst().getName());
        beanE.setValue(beanAFirst().getValue());
        return beanE;
    }

    @Bean("beanESecond")
    public BeanE beanESecond() {
        BeanE beanE = new BeanE();
        beanE.setName(beanASecond().getName());
        beanE.setValue(beanASecond().getValue());
        return beanE;
    }

    @Bean("beanEThird")
    public BeanE beanEThird() {
        BeanE beanE = new BeanE();
        beanE.setName(beanAThird().getName());
        beanE.setValue(beanAThird().getValue());
        return beanE;
    }

}
