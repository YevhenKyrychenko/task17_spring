package com.kyrychenko.task17_spring.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanPostProcessor;

public class MyBeanPostProcessor implements BeanPostProcessor {
    private String name;

    @Value("ZZZ")
    public void setName(String name) {
        this.name = name;
        System.out.println("MyBeanPostProcessor setName() "+name);
    }

    public MyBeanPostProcessor() {
        System.out.println("MyBeanPostProcessor Constructor()");
    }

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) {
        System.out.println("postProcessBeforeInitialization()");
        System.out.println("   >>bean=" + bean + " beanName=" + beanName);
        return bean;
    }

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) {
        System.out.println("postProcessAfterInitialization()");
        System.out.println("   >>bean=" + bean + " beanName=" + beanName);
        return bean;
    }
}
