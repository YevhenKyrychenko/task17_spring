package com.kyrychenko.task17_spring.beans;

import org.springframework.validation.Errors;

public interface BeanValidator {
    void validate(Object obj, Errors e);
}
