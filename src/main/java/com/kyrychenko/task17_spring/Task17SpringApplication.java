package com.kyrychenko.task17_spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
public class Task17SpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task17SpringApplication.class, args);
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Task17SpringApplication.class);

		for (String beanName : context.getBeanDefinitionNames()) {
			System.out.println(beanName);
		}
	}

}
