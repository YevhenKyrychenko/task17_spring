package com.kyrychenko.task17_spring.beans;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

@Getter
@Setter
public class BeanB implements BeanValidator {
    @Value("${beanB.name}")
    private String name;
    @Value("$beanB.value")
    private int value;

    @Override
    public void validate(Object obj, Errors e) {
        ValidationUtils.rejectIfEmpty(e, "name", "name.empty");
        ValidationUtils.rejectIfEmpty(e, "value", "value.empty");
        BeanB bean = (BeanB) obj;
        if (bean.value < 0) {
            e.rejectValue("age", "negative value");
        } else if (bean.name == null) {
            e.rejectValue("name", "wrong name");
        }
    }

    private void customInitMethod() {
        System.out.println("inside customInitMethod()");
    }

    private void customDestroyMethod() {
        System.out.println("inside customDestroyMethod()");
    }

    @Override
    public String toString() {
        return "BeanB{" +
                "name='" + name + '\'' +
                ", value=" + value +
                '}';
    }
}
