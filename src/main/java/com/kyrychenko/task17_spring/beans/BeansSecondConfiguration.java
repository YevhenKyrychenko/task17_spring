package com.kyrychenko.task17_spring.beans;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class BeansSecondConfiguration {
    @Bean
    @Lazy(true)
    public BeanF beanF() {
        return new BeanF();
    }
}
